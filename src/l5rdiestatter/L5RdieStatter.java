/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package l5rdiestatter;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author telok @ http://www.giantitp.com/forums/
 */
public class L5RdieStatter {

    private static Scanner keyIn = new Scanner(System.in);
    private static Random randy = new Random();
    
    public static void main(String[] args) {
        
        boolean loopLimit = true;
        while(loopLimit)
        {
            System.out.println("Format: X Y");
            System.out.println("Required: X = R type dice to roll, must be from 1 to 9");
            System.out.println("Required: Y = S type dice to roll, must be from 0 to 9");
            String answer = keyIn.nextLine();
            if (answer.matches("\\d \\d")){
                int rDie = Integer.decode(answer.substring(0, 1));
                int sDie = Integer.decode(answer.substring(2));
                doStats(rDie, sDie);
            }
            else {
                loopLimit = false;
            }
        }
        
    }

    private static String rollR(int limit) {
        // rolls a single ring die
        String out = "";
        
        if (limit < 100){
            
            int dieSide = randy.nextInt(6) + 1;
            // X = nil, S = success, s = strife, O = opportunity, E = explodes
            
            if (dieSide == 1){
                out = "X,";
            }
            else if (dieSide == 2){
                out = "S,";
            }
            else if (dieSide == 3){
                out = "Ss,";
            }
            else if (dieSide == 4){
                limit ++;
                out = rollR(limit) + "Es,*";
            }
            else if (dieSide == 5){
                out = "O,";
            }        
            else {
                out = "Os,";
            }
        }
        
        return out;
    }
    
    private static String rollS(int limit) {
        // rolls a single skill die
        String out = "";
        
        if (limit < 100){
                    
            int dieSide = randy.nextInt(12) + 1;
            // X = nil, S = success, s = strife, O = opportunity, E = explodes
        
            if (dieSide <= 2){
                out = "X,";
            }
            else if (dieSide <= 4){
                out = "S,";
            }
            else if (dieSide == 5){
                out = "SO,";
            }
            else if (dieSide <= 7){
                out = "Ss,";
            }
            else if (dieSide == 8){
                limit++;
                out = rollS(limit) + "E,*";
            }        
            else if (dieSide == 9){
                limit++;
                out = rollS(limit) + "Es,*";
            }
            else {
                out = "O,";
            }
        }

        return out;
    }
    
        private static int countInRolls(String input, String searchFor){
        // returns number of instances of searchFor in the input
        int count = 0;

        for (int i = 0; i < input.length(); i++){          
            if (input.substring(i, i+1).equals(searchFor)){
                count++;
            }
        }
        return count;
    }
    
    private static String rollDice(int rDie, int sDie){
        // rolls a number of dice
        String output = "";
        
        for (int i = 0; i < rDie; i++){
            output = output + rollS(0);
        }
        for (int i = 0; i < sDie; i++){
            output = output + rollS(0);
        }
        
        return output;
    }
    
    private static int[][] doSort(int[][] arrayIn){
        // sort the array on first column
        Arrays.sort(arrayIn, new Comparator<int[]>() {
            @Override
            public int compare(int[] a, int[] b) {
            return Integer.compare(a[0], b[0]);
            }
        });
        return arrayIn;
    }
    
    private static void displayResults(int[][] arrayIn){
        // count 0 successes, % of total
        // for each # strife & each # opps make a grid of output
        // format "[%, S, O]"
        int maxSuccess = arrayIn[arrayIn.length -1][0];
        int length = arrayIn.length;
        int[] maxSuccessCounter = new int[maxSuccess+1]; // remember, zero successes
        for(int a = 0; a < length; a++){
            // fill success counter
            maxSuccessCounter[arrayIn[a][0]]++;
        }
        
        int maxStrife = 0;
        for(int b = 0; b < length; b++){
            if(arrayIn[b][1] > maxStrife){
                maxStrife = arrayIn[b][1];
            }
        }
         
        int maxOpp = 0;
        for(int c = 0; c < length; c++){
           if(arrayIn[c][2] > maxOpp){
               maxOpp = arrayIn[c][2];
           }
        }
        
        double probabilitySum = 0;
        int successCountIndex = 0;
        int stressCountIndex = 0;
        int oppCountIndex = 0;
        int successCount = 0;
        int stressCount = 0;
        int oppCount = 0;
        
        for(int d = 0; d <= maxSuccess; d++){
            double percentage = (double)maxSuccessCounter[d] / (double)length;
            if(probabilitySum == 0){
                probabilitySum = percentage;
            }else{
                probabilitySum = probabilitySum + percentage;
            }
            
            int[] tempArray = checkCounts(arrayIn, successCountIndex, d, -1, -1, true, false, false);
            successCount = tempArray[0];
            successCountIndex = tempArray[1];
            
            System.out.printf("Successes %1.0f : number %1.0f : probability %.2f : met or exceeded %.2f %n", (double)d, (double)successCount, percentage, 1-(probabilitySum));
            for(int e = 0; e <= maxStrife; e++){
                
                tempArray = checkCounts(arrayIn, stressCountIndex, d, e, -1, false, true, false);
                stressCount = tempArray[0];
                
                //output strife row
                percentage = (double)stressCount / (double)successCount;
                System.out.printf("Strife %1.0f : number %1.0f : prob. %.2f || ", (double)e, (double)stressCount, percentage);
                for(int f = 0; f <= maxOpp; f++){
                    
                    tempArray = checkCounts(arrayIn, oppCountIndex, d, e, f, false, false, true);
                    oppCount = tempArray[0];
                    
                    percentage = (double)oppCount / (double)stressCount;
                    System.out.printf("Opp %1.0f : count %1.0f : prob. %.2f :: ", (double)f, (double)oppCount, percentage);
                }
                oppCountIndex = successCountIndex;
                System.out.println("");
            }
            stressCountIndex = successCountIndex;
            System.out.println("");
        }

    }
    
    private static int[] checkCounts(int[][] arrayIn, int startIndex, int succGoal, 
            int strGoal, int oppGoal, boolean successing, boolean stressing
            , boolean opping){
        // counts the number of goal matches after an index
        // includes early exit so we don't search the whole array each time
        // REQUIRES if(successing):succGoal;if(stressing):succGoal && strGoal;
        // REQUIRES if(opping):succGoal && strGoal && oppGoal
        
        // array indexes
        // 0. successes
        // 1. strife
        // 2. opportunity
        
        int[] output = new int[2]; 
        // 1 is count, 2 is new index
        int successCount = 0;
        
        if(successing){
            while(startIndex < arrayIn.length && arrayIn[startIndex][0] <= succGoal){
                if(arrayIn[startIndex][0] == succGoal){
                    successCount++;
                } else if (arrayIn[startIndex][0] > succGoal){
                    startIndex = arrayIn.length;
                }
                startIndex++;
            }
        }else if (stressing){
            while(startIndex < arrayIn.length && arrayIn[startIndex][0] <= succGoal){
                if(arrayIn[startIndex][1] == strGoal){
                    successCount++;
                } else if (arrayIn[startIndex][0] > succGoal){
                    // uses success goal as cut off point
                    startIndex = arrayIn.length;
                }
                startIndex++;
            }
        }else if (opping){
            while(startIndex < arrayIn.length && arrayIn[startIndex][0] <= succGoal){
                if(arrayIn[startIndex][1] == strGoal
                        && arrayIn[startIndex][2] == oppGoal){
                    successCount++;
                } else if (arrayIn[startIndex][0] > succGoal){
                    // uses success goal as cut off point
                    startIndex = arrayIn.length;
                }
                startIndex++;
            }
        }

        output[0] = successCount;
        output[1] = startIndex;
        
        return output;
    }
    
    private static void doStats(int rDie, int sDie){
        
        int max = 1000000;
        String[] firstArray = new String[max];

        // get raw rolls
        for (int i = 0; i < max; i++){
            randy.setSeed(System.currentTimeMillis()+(i*27)); // yes, I'm lazy
            firstArray[i] = rollDice(rDie, sDie);
        }
        
        // output array types
        // 0. success minimum strife
        // 1. success minimum strife using opportunity to negate
        // 2. success maximum opportunity
        // 3. opportunity minimum strife
        // 4. opportunity minimum strife strife using opportunity to negate
        // 5. opportunity maximum success
        // 6. maximum successes
        
        // output array indexes
        // 0. successes
        // 1. strife
        // 2. opportunity
        
        System.out.println("Assessing " + max + " rolls");
        System.out.println("Success with minimum strife");
        int[][] processedArray = new int[max][3];
        for(int i = 0; i < max; i++){
            int[] temp = successMinimumStrife(firstArray[i], rDie);
            processedArray[i][0] = temp[0];
            processedArray[i][1] = temp[1];
            processedArray[i][2] = temp[2];
        }
        
        displayResults(doSort(processedArray));
        
        
        
        
        
    }
    
    private static int[] successMinimumStrife (String input, int keptDice){
        // logic for counting successes with the minimum strife
        // does not use opportunity to cancel strife
        // tries to maximize opportunity after minimizing strife
        
        // output array indexes
        // 0. successes
        // 1. strife
        // 2. opportunity
        
        int dieCount = countInRolls(input, ",");
        int keepers = 0;
        int[] output = new int [3];
        output[0] = 0;
        output[1] = 0;
        output[2] = 0;
        boolean explosionKeeper = false;
        //String[] rolls = new String[dieCount];
        String[] rolls = input.split(",", -1);
        
            // 0. success minimum strife
            for(int i = 0; i < dieCount; i++){
                if(rolls[i].equals("S") || rolls[i].equals("SO")){
                    // straight success, non explosion
                    output[0]++;
                    keepers++;
                    if(rolls[i].equals("SO")){
                        output[2]++;
                    }
                    rolls[i] = ""; // kept this die so empty this cell
                }
                if(rolls[i].equals("E") || rolls[i].equals("Es")){
                    // explosion success
                    if(rolls[i].equals("E")){
                        output[0]++;
                        explosionKeeper = true;
                        rolls[i] = ""; // kept this die so empty this cell
                    }
                    i++;
                    while(i < rolls.length && rolls[i].contains("*")){
                        if(rolls[i].equals("*S") || rolls[i].equals("*SO") 
                                || rolls[i].equals("*E")){
                            output[0]++;
                            if(rolls[i].equals("*SO")){
                                output[2]++;
                            }
                            if (explosionKeeper == false){
                                explosionKeeper = true;
                            }
                            rolls[i] = ""; // kept this die so empty this cell
                        }
                        i++;
                    }
                    if (explosionKeeper){
                        keepers++;
                    }
                    explosionKeeper = false;
                    // end of explosion check, reset explosionKeeper
                }
            }
            // end of successes check
            // check if meet kept dice & add more success dice
            if(keepers < keptDice){
                for(int i = 0; i < dieCount; i++){
                    if(rolls[i].contains("Ss") || rolls[i].contains("Es")){
                        output[0]++;
                        output[1]++;
                        keepers++;
                        rolls[i] = "";
                    }
                }
            }
            if(keepers < keptDice){
                for(int i = 0; i < dieCount; i++){
                    if(rolls[i].equals("O") || rolls[i].equals("*O")){
                        output[2]++;
                        keepers++;
                        rolls[i] = "";
                    } 
                }
            }
            if(keepers < keptDice){
                for(int i = 0; i < dieCount; i++){
                    if(rolls[i].equals("Os") || rolls[i].equals("*Os")){
                        output[1]++;
                        output[2]++;
                        keepers++;
                        rolls[i] = "";
                    } 
                }
            }
        return output;
    }
    
    
    

    
}
